#!/usr/bin/env python
import sys

FILE_NAME = ".notes.txt"

def delete_note(num: int):
    try:
        f = open(FILE_NAME, 'r')
        lines = f.readlines()
        f.close()
        f = open(FILE_NAME, 'w')
        for i, line in enumerate(lines):
            if i != num:
                f.write(line)
        f.close()
    except:
        print("There isn't notes :)")
    return 0

def main():
    if len(sys.argv)<2 or not sys.argv[1].isdigit():
        print("remnote <note_number>")
        return 1
    else:
        delete_note(int(sys.argv[1]))
    return 0

if __name__ == "__main__":
    main()
