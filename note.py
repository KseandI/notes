#!/usr/bin/env python
import sys

FILE_NAME = ".notes.txt"

def read_notes():
    try:
        f = open(FILE_NAME, "r")
        for i, note in enumerate(f.readlines()):
            print(i, note, end='')
    
        f.close()
    except:
        print("No notes :3")
        return 1
    return 0

def write_note(note):
    f = open(FILE_NAME, "a+")
    if len(f.readlines())>0:
        f.write('\n')
    f.write(note)
    f.write('\n')
    f.close()
    return 0

def main():
    if len(sys.argv)<2:
        read_notes()
    else:
        write_note(" ".join(sys.argv[1:]))
    return 0

if __name__ == "__main__":
    main()
